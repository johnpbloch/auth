<?php

namespace App\Http\Middleware;

use Closure;

class EnsureHttps {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		app( 'url' )->forceSchema( 'https' );

		return $next( $request );
	}
}
